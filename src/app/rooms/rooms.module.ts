import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoomsListComponent } from './rooms-list/rooms-list.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [RoomsListComponent],
  declarations: [RoomsListComponent]
})
export class RoomsModule { }
